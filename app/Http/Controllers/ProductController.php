<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Response;
use DB;
use File;
use Auth;

 
class ProductController extends Controller
{
	
  public function product_Add_From()
  {
    return view('product_form');
  }
  public function product_Add_submit(Request $request){
			$productimage = $request->file('product_img');
			$Img=rand() . '.' . $productimage->getClientOriginalExtension();
  	 $productimage->move(public_path('product'),$Img);
  	 $Product = new Product;
    $Product->Category=$request->cname;
    $Product->user_id=Auth::user()->id;
    $Product->Name=$request->pname;            
    $Product->Img=$Img;
    $Product->Qty=$request->qty;
    $Product->Price=$request->price;
    $Product->SKU=$request->sku;
    $Product->save(); 
  	return redirect('product-list');
  }
  public function product_list(){
  	 
        $Product=Product::where('user_id',Auth::user()->id)->get();
        return view('product_list',compact('Product'));
 
  }
  public function update_list(){
    return view('Update_form');
  }
  public function update_list_submit(Request $request){
    $Product=Product::find($request->pid);
    
    $productimage = $request->file('product_img');
      $Img=rand() . '.' . $productimage->getClientOriginalExtension();
     $productimage->move(public_path('product'),$Img);
   
    $Product->Category=$request->cname;
    $Product->Name=$request->pname;            
    $Product->Img=$Img;
    $Product->Qty=$request->qty;
    $Product->Price=$request->price;
    $Product->SKU=$request->sku;
    $Product->save(); 

    return redirect('product-list');
  }
  public function update_edit(Request $request){
  $Product=Product::where('id',$request->id)->first();
        return view('Update_form',compact('Product'));
  }
}

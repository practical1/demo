<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {    
    return redirect('/login');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/product-add','ProductController@product_Add_From')->name('product_add');
Route::post('/product-add-submit','ProductController@product_Add_submit')->name('product_add_submit');
Route::get('/product-list','ProductController@product_list')->name('product_list');
Route::get('/product-edit/{id}','ProductController@update_edit')->name('update_edit');
Route::post('/product-update-list','ProductController@update_list')->name('upadate_list');
Route::post('/update-list-submit','ProductController@update_list_submit')->name('update_list_submit');

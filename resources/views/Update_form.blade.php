<!DOCTYPE html>
<html>
<head>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <title></title>
</head>
<body>
	<center>
		<h1>Update Product Details</h1>
         <form  name="profileForm" method="post" id="profileForm" action="{{ route('update_list_submit')}}"   enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
          <input type="hidden" name="pid" value="{{$Product->id}}" /> 

           <table border="2">
				<tr>
					<td>category</td>
					<td><input type="text" name="cname" value="{{$Product->Category}}"  /></td>
				</tr>
				<tr>
					<td>Product</td>
					<td><input type="text" name="pname" value="{{$Product->Name}}"   /></td>
				</tr>
				<tr>
					<td>price</td>
					<td>
						<input type="text" name="price" value="{{$Product->Price}}"  /> 
					</td>
				</tr>
				<tr>
					<td>SKU</td>
					<td>
						<input type="text" name="sku" value="{{$Product->SKU}}"  /> 
					</td>
				</tr>
					<td>QTY</td>
					<td>
						<input type="text" name="qty" value="{{$Product->Qty}}"  /> 
					</td>
				</tr>
				<tr>
					<td>Image</td>
					<td>
						<input type="file" name="product_img"   /> 
					</td>
				</tr>
				<tr>
					 
					<td colspan="2" align="center"><input type="submit" name="submit" value="Update Data"></td>
	               
					 
				</tr>
			</table>
			 </form>        
            <!-- /.row -->
          </div>           
        </div>   

        <!-- /.row -->
      </div>
</center>
</body>
</html>